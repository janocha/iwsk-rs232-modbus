package pl.polsl.view.gui.controllers;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gnu.io.SerialPort;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import pl.polsl.model.Master;
import pl.polsl.model.MasterModbusExecutor;
import pl.polsl.model.ModbusFrame;
import pl.polsl.model.RS232;
import pl.polsl.model.Slave;
import pl.polsl.model.SlaveModbusExecutor;

public class MenuController {
	private MainController mainController;
	private File csvDirectoryPath = null;
	// !!modbus
	Master master;
	Slave slave;
	Thread slaveThread;
	@FXML
	private ComboBox comboBoxWyborPortu;
	@FXML
	private ComboBox comboBoxSzybkoscTransmisji;
	@FXML
	private ComboBox comboBoxTimeoutTransakcji;
	@FXML
	private ComboBox comboBoxTimeoutZnaku;
	@FXML
	private ComboBox comboBoxAdresStacji;
	@FXML
	private ComboBox comboBoxIloscRetransmisji;
	@FXML
	private ComboBox comboBoxAdresOdbiorcy;
	@FXML
	private ComboBox comboBoxKodRozkazu;
	@FXML
	private RadioButton radioButtonMaster;
	@FXML
	private RadioButton radioButtonSlave;
	@FXML
	private Button skanujPort;
	@FXML
	private Button buttonPolacz;
	@FXML
	private Button buttonRozlacz;
	@FXML
	private Label labelAdresStacji;
	@FXML
	private Label labelIloscRetransmisji;
	@FXML
	private GridPane gridPaneUstawieniaAdresKod;

	@FXML
	private TextArea textAreaWyswietlana;
	@FXML
	private TextArea textAreaWyslij;
	@FXML
	private TextField textFieldRamkaWyslana;
	@FXML
	private TextField textFieldRamkaOdebrana;
	@FXML
	private Button buttonWyslij;

	// !!modbus

	@FXML
	private Button buttonStart;
	@FXML
	private Button buttonDetails;

	@FXML
	public void initialize() {

		refreshSerialPorts();
		comboBoxSzybkoscTransmisji.getItems().addAll("9600", "4800", "2400", "1200", "600", "300");
		comboBoxSzybkoscTransmisji.getSelectionModel().selectFirst();
		comboBoxTimeoutTransakcji.getItems().addAll(generateSuccessiveNumbers(0.1d, 10.0d, 0.1d));
		comboBoxTimeoutTransakcji.getSelectionModel().selectFirst();
		comboBoxTimeoutZnaku.getItems().addAll(generateSuccessiveNumbers(0.01d, 10.0d, 0.01d));
		comboBoxTimeoutZnaku.getSelectionModel().selectFirst();
		comboBoxAdresStacji.getItems().addAll(generateSuccessiveNumbers(1, 247, 1));
		comboBoxAdresStacji.getSelectionModel().selectFirst();
		comboBoxIloscRetransmisji.getItems().addAll(generateSuccessiveNumbers(0, 5, 1));
		comboBoxIloscRetransmisji.getSelectionModel().selectFirst();
		actionSelectSlave(null);
		comboBoxAdresOdbiorcy.getItems().addAll(generateSuccessiveNumbers(0, 247, 1));
		comboBoxAdresOdbiorcy.getSelectionModel().selectFirst();
		comboBoxKodRozkazu.getItems().addAll("1", "2");
		comboBoxKodRozkazu.getSelectionModel().selectFirst();
	}
 @Override
	  public void finalize() {
		viewMessageOnScreen("Error", null, "Wybrany port jest w tej chwili niedostepny.\n"
				+ "Sprawd� czy inne programy nie u�ywaj� portu " + ".", AlertType.ERROR);
	   Platform.exit();
	  }
	private void refreshSerialPorts() {
		comboBoxWyborPortu.getItems().clear();
		comboBoxWyborPortu.getItems().addAll(RS232.getSerialPortList());
		comboBoxWyborPortu.getSelectionModel().selectFirst();
	}

	private List<String> generateSuccessiveNumbers(int firstNumber, int lastNumber, int step) {
		int tmp = firstNumber;
		List<String> list = new ArrayList();
		list.clear();
		while (tmp <= lastNumber) {
			list.add(String.valueOf(tmp));
			tmp += step;
		}
		return list;
	}

	private List<String> generateSuccessiveNumbers(double firstNumber, double lastNumber, double step) {
		double tmp = firstNumber;
		List<String> list = new ArrayList();
		list.clear();
		while (tmp <= lastNumber) {
			list.add(String.valueOf(tmp));
			tmp += step;
			tmp = Math.round(tmp * 100.0) / 100.0;
		}
		return list;
	}

	private void viewMessageOnScreen(String title, String header, String text, AlertType type) {
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(text);
		alert.showAndWait();
	}

	@FXML
	void actionSkanujPort(ActionEvent event) {
		refreshSerialPorts();
	}

	@FXML
	void actionSelectMaster(ActionEvent event) {
		labelAdresStacji.setVisible(false);
		comboBoxAdresStacji.setVisible(false);
		labelIloscRetransmisji.setVisible(true);
		comboBoxIloscRetransmisji.setVisible(true);
	}

	@FXML
	void actionSelectSlave(ActionEvent event) {
		labelAdresStacji.setVisible(true);
		comboBoxAdresStacji.setVisible(true);
		labelIloscRetransmisji.setVisible(false);
		comboBoxIloscRetransmisji.setVisible(false);
	}

	@FXML
	void actionPolacz(ActionEvent event) {
		// pobieranie danych z combobox'ow
		String wybranyPort = comboBoxWyborPortu.getValue().toString();
		int szybkoscTransmisji = Integer.parseInt(comboBoxSzybkoscTransmisji.getValue().toString());
		int timeoutTransakcji = (int) (Double.parseDouble(comboBoxTimeoutTransakcji.getValue().toString()) * 1000);
		int timeoutZnaku = (int) (Double.parseDouble(comboBoxTimeoutZnaku.getValue().toString()) * 1000);
		int adresStacji = Integer.parseInt(comboBoxAdresStacji.getValue().toString());
		boolean isMaster = radioButtonMaster.isSelected();
		int iloscRetransmisji = Integer.parseInt(comboBoxIloscRetransmisji.getValue().toString());

		// dalsze ustawienia transmisji
		int stopBits = SerialPort.STOPBITS_1;
		int dataBits = SerialPort.DATABITS_7;
		int parity = SerialPort.PARITY_EVEN;

		RS232 rs = new RS232();

		boolean isOpen = rs.openPort(wybranyPort, null, szybkoscTransmisji, dataBits, stopBits, parity, 0);
		if (isOpen) {
			viewMessageOnScreen("Info", null, "Po��czono", AlertType.INFORMATION);
		} else {
			viewMessageOnScreen("Error", null, "Wybrany port jest w tej chwili niedostepny.\n"
					+ "Sprawd� czy inne programy nie u�ywaj� portu " + wybranyPort + ".", AlertType.ERROR);
			return;
		}
		if (isMaster) {
			master = new Master(iloscRetransmisji, timeoutTransakcji, timeoutZnaku);
			MasterModbusExecutor masterExecutor = new MasterModbusExecutor();
			master.setMasterExecutor(masterExecutor);
			master.setRs(rs);
			gridPaneUstawieniaAdresKod.setVisible(true);
		} else {
			slave = new Slave(timeoutZnaku);
			SlaveModbusExecutor slaveExecutor = new SlaveModbusExecutor((byte) adresStacji);
			slave.setSlaveExecutor(slaveExecutor);
			slave.setRs(rs);
			slaveThread = new Thread(slave);
			slaveThread.start();
		}

		buttonPolacz.setVisible(false);
		buttonRozlacz.setVisible(true);
	}

	@FXML
	void actionRozlacz(ActionEvent event) {

		if (master != null) {
			master.getRs().closePort();
			master = null;
			gridPaneUstawieniaAdresKod.setVisible(false);
		}
		if (slave != null) {
			slaveThread.stop();
			slave.getRs().closePort();
			slave = null;
		}

		buttonPolacz.setVisible(true);
		buttonRozlacz.setVisible(false);
	}

	@FXML
	void actionWyslij(ActionEvent event) {
		try
		{
			byte adres = Byte.parseByte(comboBoxAdresOdbiorcy.getValue().toString());
			byte kodRozkazu = Byte.parseByte(comboBoxAdresOdbiorcy.getValue().toString());
			
			ModbusFrame request = new ModbusFrame();
			request.setAddress(adres);
			request.setFunction(kodRozkazu);
			request.setData(textAreaWyslij.getText().getBytes());
			if(!master.send(request)) {
				textAreaWyswietlana.setText("Transakcja nieudana\r\n");
			}
		}
		catch(Exception e) {
			e.getMessage();
		}
	}
	///////////////////////////////////////////////////////////////////////////////

	@FXML
	void exit(ActionEvent event) {
		Platform.exit();
	}

	@FXML
	void about(ActionEvent event) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("About");
		alert.setHeaderText("IWSK - modbus");
		alert.setContentText("v1.0, 27.09.2017r.\n" + "Informatyka sem.6, AEiI \n" + "Rybnik - sekcja 2\n");

		alert.showAndWait();
	}

	@FXML
	void openApplication(ActionEvent event) {

		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/AppScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		AppController appController = loader.getController();
		appController.setMainController(mainController);
		mainController.setScreen(pane);
	}

	@FXML
	void openOptions(ActionEvent event) {

	}

	public void setMainController(MainController mainController) {
		this.mainController = mainController;
	}

	@FXML
	void start(ActionEvent event) {

	}

	@FXML
	void details(ActionEvent event) {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/AppScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		AppController appController = loader.getController();
		appController.setMainController(mainController);
		mainController.setScreen(pane);
	}
}
